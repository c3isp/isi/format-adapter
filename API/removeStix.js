/*
 * File: removeStix.js
 * File Created: Thursday, 31st May 2018 11:29:55 am
 * Author: Rihab Ben Hamouda (rihab.benh@gripdocket.com)
 * -----
 * Last Modified: Thursday, 11th April 2019 04:01:34 pm
 * Modified By: Rihab Ben Hamouda (rihab.benh@gripdocket.com)
 * -----
 * Copyright 2018 GridPocket, GridPocket
 */


const fs = require('fs');
const express = require('express');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../swagger.json');
const multer = require('multer');
const os = require('os');
const logs = require('../Logs/sendLogs.js');
const csv = require('../Converter/CEFtoCSV.js');

const upload = multer({ dest: 'uploads/' });
const router = express.Router();

const post = function removeStix(req, res) {
  logs.sendLogs(req, 0);
  const { path } = req.file;
  fs.readFile(path, 'utf8', (err, data) => {
    if (err) {
      logs.sendLog(req, 1, 404);
      return res.sendStatus(404);
    }
    if (path) {
      try {
        const json = JSON.parse(data);
        let objects;
        if (!json.objects) {
          logs.sendLogs(req, 1, 400);
          return res.sendStatus(400);
        } else if (req.query.CSV === 'true') {
          fs.unlinkSync(path);
          const result = csv.convertToCSV(json);
          if (result == null) {
            logs.sendLogs(req, 1, 400);
            return res.sendStatus(400);
          }
          logs.sendLogs(req, 1, 200);
          return res.send(result);
        } else if (!json.objects[0].cybox) {
          objects = JSON.stringify(json.objects);
        } else {
          objects = json.objects[0].cybox.objects[0].items[0];
        }
        if (Array.isArray(objects)) {
          objects = objects.join(os.EOL);
        }
        fs.unlinkSync(path);
        res.setHeader('content-type', 'text/plain');
        logs.sendLogs(req, 1, 200);
        return res.send(objects);
      } catch (e) {
        console.log(e);
        fs.unlinkSync(path);
        res.setHeader('content-type', 'text/plain');
        logs.sendLogs(req, 1, 400);
        return res.sendStatus(400);
      }
    }
  });
};

router.route('/convertDL')
  .post(upload.single('file'), post);

router.use('/format-adapter/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
router.use('/format-adapter/api/v1', router);

module.exports = router;
