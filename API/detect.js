/**
 * File: detect.js
 * File Created: Tuesday, 10th July 2018 10:46:07 am
 * Author: Rihab Ben Hamouda (rihab.benh@gripdocket.com)
 * -----
 * Last Modified: Monday, 3rd December 2018 11:44:51 am
 * Modified By: Rihab Ben Hamouda (rihab.benh@gripdocket.com)
 * -----
 * GridPocket SAS Copyright (C) 2018 All Rights Reserved
 * This source is property of GridPocket SAS.
 * Please email contact@gridpocket.com for more information.
 */

const fs = require('fs');
const express = require('express');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../swagger.json');
const multer = require('multer');

const logs = require('../Logs/sendLogs.js');

const auth = require('../Converter/authLog.js');
const report = require('../Converter/ReportToJSON');
const firewall = require('../Converter/firewallToCEF');
const antiMalware = require('../Converter/antiMalwareToCEF');
const dataModel = require('../Converter/dataModel');
const email = require('../Converter/EmailToJSON');
const csv = require('../Converter/csvToCef');
const idea = require('../Converter/IDEAtoSTIX');

const upload = multer({ dest: 'uploads/' });
const router = express.Router();

const post = function convert(res, req) {
  logs.sendLogs(res, 0);
  new Promise((resolve, reject) => {
    const { path } = res.file;
    fs.readFile(path, 'utf8', (err, file) => {
      if (err) {
        logs.sendLogs(1, 404);
        return reject(req.sendStatus(404));
      } else if (res.file.originalname.endsWith('.html')) {
        report.convertToJSON(res, req);
        logs.sendLogs(res, 1, 200);
      } else if (res.file.originalname.includes('Firewall') || res.file.originalname.includes('firewall')) {
        firewall.convertToJSON(res, req);
        logs.sendLogs(res, 1, 200);
      } else if (res.file.originalname.includes('anti_malware') || res.file.originalname.includes('Anti_malware') || res.file.originalname.includes('Anti-Malware')) {
        antiMalware.convertToJSON(res, req);
        logs.sendLogs(res, 1, 200);
      } else if (file.startsWith('category') || file.startsWith('timestamp')) {
        dataModel.convertToJSON(res, req);
        logs.sendLogs(res, 1, 200);
      } else if (res.file.originalname.endsWith('.eml') || file.startsWith('Return-Path:') || file.startsWith('Delivered-To:')) {
        email.convertToJSON(res, req);
        logs.sendLogs(res, 1, 200);
      } else if (file.startsWith('#dataType=netflow') || file.startsWith('#dataType=dns')) {
        csv.convertToJSON(res, req);
        logs.sendLogs(res, 1, 200);
      } else if (file.startsWith('#dataType=ssh')) {
        auth.convertToJSON(res, req);
        logs.sendLogs(res, 1, 200);
      } else if (res.file.originalname.endsWith('.idea')) {
        idea.convertToJSON(res, req);
        logs.sendLogs(res, 1, 200);
      } else if (file.startsWith('{')) {
        const json = JSON.parse(file);
        fs.unlinkSync(path);
        logs.sendLogs(res, 1, 200);
        req.json(json);
        return resolve();
      } else {
        fs.unlinkSync(path);
        logs.sendLogs(res, 1, 400);
        return req.sendStatus(400);
      }
      return resolve();
    });
  });
};

router.route('/convert')
  .post(upload.single('file'), post);

router.use('/format-adapter/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
router.use('/format-adapter/api/v1', router);

module.exports = router;
