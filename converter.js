/*
 * File: converter.js
 * File Created: Friday, 11th May 2018 10:30:56 am
 * Author: Rihab Ben Hamouda (rihab.benh@gripdocket.com)
 * -----
 * Last Modified: Thursday, 31st May 2018 11:43:29 am
 * Modified By: Rihab Ben Hamouda (rihab.benh@gripdocket.com)
 * -----
 * Copyright 2018 GridPocket, GridPocket
 */

const fs = require('fs');
const express = require('express');
const https = require('https');
const swagger = require('./Config/GenerateSwaggerConfig');
const springCloudConfig = require('spring-cloud-config');
const commandLineArgs = require('command-line-args');

const optionDefinitions = [
  { name: 'env', type: String },
];

const options = commandLineArgs(optionDefinitions);

const env = options.env || process.env.SPRING_PROFILES_ACTIVE || 'testbed';

const configOptions = {
  configPath: './format-adapter/Config',
  activeProfiles: [env],
  level: 'debug',
};

console.log(`Current environment : ${env}`);

// Get Spring Cloud Config paramaters
springCloudConfig.load(configOptions)
  .then((config) => {
    const app = express();

    swagger.generateSwaggerConfig(config.server.hostname, config.server.port, config.server.scheme);

    app.use((require('./API/removeStix')));
    app.use((require('./API/detect')));

    exports.auditManagerUrl = config.css.auditmanagerurl;

    const privateKey = fs.readFileSync(config.isi.privateKey, 'utf8');
    const certificate = fs.readFileSync(config.isi.cert, 'utf8');

    const credentials = { key: privateKey, cert: certificate };

    const httpsServer = https.createServer(credentials, app);

    httpsServer.listen(config.server.port);
  })
  .catch((error) => { console.log(error); });
