/**
 * File: GenerateSwaggerConfig.js
 * File Created: Monday, 13th May 2019 10:04:28 am
 * Author: Rihab Ben Hamouda (rihab.benh@gripdocket.com)
 * -----
 * Last Modified: Monday, 13th May 2019 03:05:18 pm
 * Modified By: Rihab Ben Hamouda (rihab.benh@gripdocket.com)
 * -----
 * GridPocket SAS Copyright (C) 2019 All Rights Reserved
 * This source is property of GridPocket SAS.
 * Please email contact@gridpocket.com for more information.
 */

const fs = require('fs');

exports.generateSwaggerConfig = (host, port, scheme) => {
  const file = `{
        "swagger": "2.0",
        "info": {
            "version": "1.0.0",
            "title": "Converter",
            "description": "API to convert CTI data into CEF or JSON Format with a STIX layer"
        },
        "host": "${host}:${port}",
        "basePath": "/format-adapter/api/v1",
        "schemes": [
            "${scheme}"
        ],
        "paths": {
            "/convert": {
                "post": {
                    "summary": "Convert CTI data to the right format and add STIX layer",
                    "tags": [
                        "Format Adapter"
                    ],
                    "consumes": [
                        "multipart/form-data"
                    ],
                    "produces": [
                        "application/json"
                    ],
                "parameters": [
                    {
                        "name": "file",
                        "in": "formData",
                        "required": true,
                        "description": "File to convert",
                        "type": "file"
                    }
                ],
                    "responses": {
                        "200": {
                            "description": "OK"
                        },
                        "404": {
                            "description": "File not found"
                        },
                        "400": {
                            "description": "Format not recognised"
                        },
                        "500": {
                            "description": "An error has occured"
                        }
                    }
                }
            },
            "/convertDL": {
                "post": {
                    "summary": "Remove STIX layer",
                    "tags": [
                        "Format Adapter"
                    ],
                    "consumes": [
                        "multipart/form-data"
                    ],
                    "produces": [
                        "text/plain"
                    ],
                "parameters": [
                    {
                        "name": "file",
                        "in": "formData",
                        "required": true,
                        "description": "File to unStix",
                        "type": "file"
                    }, 
                    {
                        "in": "query",
                        "name": "CSV",
                        "required": false,
                        "type": "boolean"
                    }
                ],
                    "responses": {
                        "200": {
                            "description": "OK"
                        },
                        "400": {
                            "description": "The file is not a JSON file with a stix"
                        },
                        "404": {
                            "description": "File not found"
                        },
                        "500": {
                            "description": "An error has occured"
                        }
                    }
                }
            }
        },
        "definitions": {
            "convert": {
              "required": [
                "file"
              ],
              "properties": {
                "file": {
                  "type": "file"
                }
              }
            },
            "convertDL": {
                "required": [
                  "file"
                ],
                "properties": {
                  "file": {
                    "type": "file"
                  }
                }
              }
        }
    }`;
  fs.writeFileSync('./format-adapter/swagger.json', file);
};
