/**
 * File: IDEAtoStix.js
 * File Created: Friday, 8th Avril 2019 10:04:46 am
 * Author: Rihab Ben Hamouda (rihab.benh@gripdocket.com)
 * -----
 * Last Modified: Wednesday, 11th Avril 2019 03:23:12 am
 * Modified By: Rihab Ben Hamouda (rihab.benh@gripdocket.com)
 * -----
 * GridPocket SAS Copyright (C) 2019 All Rights Reserved
 * This source is property of GridPocket SAS.
 * Please email contact@gridpocket.com for more information.
 */


const fs = require('fs');
const uuid = require('uuidv4');

function DomainObject(idea) {
  const node = idea.Node;
  const object = [];
  node.forEach((n) => {
    const Stix = {};
    Stix.type = 'identity';
    Stix.id = `identity—-${uuid()}`;
    if (idea.CreateTime) {
      Stix.created = idea.CreateTime;
      Stix.modified = idea.CreateTime;
    } else {
      Stix.created = idea.DetectTime;
      Stix.modified = idea.DetectTime;
    }
    Stix.name = n.Name;
    Stix.labels = n.Type;
    Stix.description = n.SW;
    Stix.identity_class = 'group';
    object.push(Stix);
  });
  return object;
}

function identityObject(idea) {
  const Stix = {};
  const category = idea.Category[0].split('.');
  if (category[0] === 'Malware') {
    Stix.type = 'malware';
    Stix.labels = category[1];
  } else if (category[0] === 'Attempt') {
    Stix.type = 'malware';
    Stix.labels = 'bot';
  } else if (category[0] === 'Intrusion') {
    Stix.type = 'malware';
    Stix.labels = 'resource-exploitation';
  } else if (category[0] === 'Vulnerable') {
    Stix.type = 'vulnerability';
  } else return {};
  Stix.id = `${Stix.type}--${uuid()}`;
  Stix.created = idea.DetectTime;
  Stix.modified = idea.DetectTime;
  Stix.name = 'unknown';
  if (idea.Ref) {
    Stix.external_references = [{ source_name: idea.Ref[0].split(':')[0], external_id: idea.Ref[0].split(':')[1] }];
  }

  return Stix;
}

function ObservedData(idea, identityID) {
  const Stix = {};
  const category = idea.Category[0].split('.');
  Stix.type = 'observed-data';
  Stix.id = `${Stix.type}--${uuid()}`;
  Stix.created_by_ref = identityID;
  Stix.created = idea.DetectTime;
  Stix.modified = idea.DetectTime;
  Stix.first_observed = idea.EventTime;
  Stix.last_observed = idea.CeaseTime;
  if (category[0] !== 'Malware' && category[0] !== 'Attempt' && category[0] !== 'Intrusion') {
    Stix.labels = [category.join('.')];
    if (idea.Timestamp) {
      Stix.created = idea.Timestamp;
    }
  }
  Stix['number-observed'] = idea.ConnCount;

  if (idea.Source || idea.Target) {
    let i = 0;
    Stix.objects = {};
    if (idea.Source && idea.Source.length > 0) {
      idea.Source.forEach((s) => {
        if (s.IP4) {
          Stix.objects[i] = { type: 'ipv4-addr', value: s.IP4.join(',') };
          i += 1;
        }
        if (s.IP6) {
          Stix.objects[i] = { type: 'ipv6-addr', value: s.IP6.join(',') };
          i += 1;
        }
      });
    }
    if (idea.Target && idea.Target.length > 0) {
      idea.Target.forEach((s) => {
        if (s.IP4) {
          Stix.objects[i] = { type: 'ipv4-addr', value: s.IP4.join(',') };
          i += 1;
        }
        if (s.IP6) {
          Stix.objects[i] = { type: 'ipv6-addr', value: s.IP6.join(',') };
          i += 1;
        }
      });
    }
    let j = 0;
    if (idea.Source && idea.Source.length > 0) {
      idea.Source.forEach((s) => {
        if (s.IP4) {
          Stix.objects[i] = {
            type: 'network-traffic',
            src_ref: `${j.toString()}`,
            protocols: ['ipv4'],
          };
        }
        if (s.IP6) {
          Stix.objects[i] = {
            type: 'network-traffic',
            src_ref: `${j.toString()}`,
            protocols: ['ipv6'],
          };
        }
        if (idea.Port) {
          Stix.objects[i].src_port = idea.Port[0];
        }
        i += 1;
        j += 1;
      });
    }
    if (idea.Target && idea.Target.length > 0) {
      idea.Target.forEach((s) => {
        if (s.IP4) {
          Stix.objects[i] = {
            type: 'network-traffic',
            dst_ref: `${j.toString()}`,
            protocols: ['ipv4'],
          };
        }
        if (s.IP6) {
          Stix.objects[i] = {
            type: 'network-traffic',
            dst_ref: `${j.toString()}`,
            protocols: ['ipv6'],
          };
        }
        if (idea.Port) {
          Stix.objects[i].dst_port = idea.Port[0];
        }
        i += 1;
        j += 1;
      });
    }
  }
  return Stix;
}
function sightingRelationship(idea, identityID, obsID, malID) {
  const Stix = {};
  Stix.type = 'sighting';
  Stix.id = `sighting--${uuid()}`;
  Stix.created_by_ref = identityID;
  Stix.created = idea.DetectTime;
  Stix.modified = idea.DetectTime;
  Stix.first_seen = idea.EventTime;
  Stix.last_seen = idea.CeaseTime;
  Stix.count = idea.ConnCount;
  Stix.sighting_of_ref = malID;
  Stix.observed_data_refs = obsID;
  Stix.where_sighted_refs = identityID;
  return Stix;
}


function createStix(idea) {
  const domain = DomainObject(idea);
  const identity = identityObject(idea);
  const observed = ObservedData(idea, domain.slice(-1)[0].id);
  const sighting = sightingRelationship(idea, domain.slice(-1)[0].id, identity.id, observed.id);

  const stix = {
    type: 'bundle',
    id: `bundle--${uuid()}`,
    spec_version: '2.0',
    objects: domain,
  };
  const category = idea.Category[0].split('.');
  if (category[0] === 'Malware' || category[0] === 'Attempt' || category[0] === 'Intrusion') {
    stix.objects.push(identity);
  }
  stix.objects.push(observed);
  stix.objects.push(sighting);

  return stix;
}

exports.convertToJSON = (req, res) => {
  new Promise((resolve, reject) => {
    const { path } = req.file;
    fs.readFile(path, 'utf8', (err, data) => {
      if (err) {
        return reject(res.sendStatus(404));
      }
      const idea = JSON.parse(data);
      const stix = createStix(idea);
      fs.unlinkSync(path);
      return resolve(stix);
    });
  }).then((stix) => {
    res.json(stix);
  })
    .catch((err) => {
      console.error('Error during process', err);
      res.status(500).send(err);
    });
};
