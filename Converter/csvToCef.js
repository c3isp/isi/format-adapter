/*
* @Author: paps
*
* GridPocket SAS Copyright (C) 2016 All Rights Reserved
* This source is property of GridPocket SAS. Please email contact@gridpocket.com for more
* information.
*
* @File name:  csvToCef.js
* @Date:   2018-04-25
* @Last Modified by:   papa.niamadio
* @Last Modified time: 2018-10-22
*
* @Description: This script enables to convert log files from CSV format to CEF format
*/

const fs = require('fs');
const crypto = require('crypto');
const eol = require('eol');

let cef = [];
let info;

function toCEF(fe) {
  fe.pop();
  if (fe[0].startsWith('#dataType=netflow')) {
    info = fe.shift();
    fe.forEach((line) => {
      const l = line.split(' ');
      for (let i = 0; i < l.length; i += 1) {
        if (l[i] === '' || l[i] === '  ') {
          l[i] = '""';
        }
      }
      let res = [];
      res[0] = `src=${l[4].split(':')[0]}`;
      res[1] = ` spt=${l[4].split(':')[1]}`;
      res[2] = ` dst=${l[6].split(':')[0]}`;
      res[3] = ` dpt=${l[6].split(':')[1]}`;
      res[4] = ` proto=${l[3]}`;
      res[5] = `end=${new Date(l[0]).getTime()}`;
      res[6] = ' dtz=Europe/Berlin,';
      res = res.toString().replace(/,/g, '').replace(/undefined/g, '""');
      cef.push(`CEF:0|${info.split(';')[5].split('=')[1]}|${info.split(';')[4].split('=')[1]}|1.0|100|netflow-${info.split(';')[2].split('=')[1]}|${info.split(';')[6].split('=')[1]}|${res}`);
    });
  } else if (fe[0].startsWith('#dataType=dns') && fe[0].includes('vendor=bind')) {
    info = fe.shift();
    fe.forEach((line) => {
      const l = line.split(' ');
      for (let i = 0; i < l.length; i += 1) {
        if (l[i] === '' || l[i] === '  ') {
          l[i] = '""';
        }
      }
      let res = [];
      res[0] = `src=${l[3].split('#')[0]}`;
      res[1] = ` spt=${l[3].split('#')[1]}`;
      res[2] = ` msg=${l[6]} ${l[7]} ${l[8]} ${l[9]} ${l[10]} `;
      res[5] = `end=${new Date(l[0]).getTime()}`;
      res[6] = ' dtz=Europe/Berlin,';
      res = res.toString().replace(/,/g, '').replace(/undefined/g, '""');
      cef.push(`CEF:0|${info.split(';')[5].split('=')[1]}|${info.split(';')[4].split('=')[1]}|1.0|100|dns-${info.split(';')[2].split('=')[1]}|${info.split(';')[6].split('=')[1]}|${res}`);
    });
  } else if (fe[0].startsWith('#dataType=dns') && fe[0].includes('vendor=unbound')) {
    info = fe.shift();
    fe.forEach((line) => {
      const l = line.split(' ');
      for (let i = 0; i < l.length; i += 1) {
        if (l[i] === '' || l[i] === '  ') {
          l[i] = '""';
        }
      }
      let res = [];
      res[0] = `src=${l[7]}`;
      res[1] = ` msg=${l[8].substring(0, l[8].length - 1)} ${l[9]} ${l[10]} `;
      res[2] = `end=${new Date(`${l[0]} ${l[1]} ${l[2]}`).setYear(new Date(info.split(';')[2].split('=')[1]).getFullYear())}`;
      res[3] = ' dtz=Europe/Berlin,';
      res = res.toString().replace(/,/g, '').replace(/undefined/g, '""');
      cef.push(`CEF:0|${info.split(';')[5].split('=')[1]}|${info.split(';')[4].split('=')[1]}|1.0|100|dns-${info.split(';')[2].split('=')[1]}|${info.split(';')[6].split('=')[1]}|${res}`);
    });
  }
}

exports.convertToJSON = (req, res) => {
  new Promise((resolve, reject) => {
    const { path } = req.file;
    fs.readFile(path, 'utf8', (err, data) => {
      if (err) {
        return reject(res.sendStatus(404));
      }
      const correctFormat = eol.lf(data);
      const fe = correctFormat.replace(/\s\s+/g, ' ').split('\n');
      toCEF(fe);
      fs.unlinkSync(path);
      return resolve();
    });
  }).then(() => {
    const json2 = {
      spec_version: '2.0',
      type: 'stix-bundle',
      id: 'stix-bundle--hash',
      objects: [
        {
          type: 'observed-data',
          id: `observed-data--${crypto.createHmac('sha1', JSON.stringify(cef)).digest('hex')}`,
          created: info.split(';')[1].split('=')[1],
          modified: info.split(';')[1].split('=')[1],
          first_observed: info.split(';')[1].split('=')[1],
          last_observed: info.split(';')[2].split('=')[1],
          cybox: {
            spec_version: '3.0',
            objects: [
              {
                type: 'array',
                minitems: 1,
                items: [cef],
              },
            ],
          },
        },
      ],
    };
    cef = [];
    json2.id = `stix-bundle--${crypto.createHmac('sha1', JSON.stringify(json2)).digest('hex')}`;
    const finalJSON = json2;
    res.json(finalJSON);
  })
    .catch((err) => {
      console.error('Error during process', err);
      res.status(500).send(err);
    });
};
