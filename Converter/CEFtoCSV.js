/*
 * File: removeStix.js
 * File Created: Tuesday, 25th June 2019 11:13:51 am
 * Author: Rihab Ben Hamouda (rihab.benh@gripdocket.com)
 * -----
 * Last Modified: Wednesday, 26th June 2019 01:52:30 pm
 * Modified By: Rihab Ben Hamouda (rihab.benh@gripdocket.com)
 * -----
 * Copyright 2018 GridPocket, GridPocket
 */

exports.convertToCSV = (stix) => {
  const CEF = stix.objects[0].cybox.objects[0].items[0];
  if (CEF[0] == null || !CEF[0].startsWith('CEF')) {
    return;
  }
  let header = '#header_version,header_deviceVendor,header_deviceProduct,header_deviceVersion,header_deviceEventClassId,header_name,header_severity';
  let csv;
  CEF.forEach((line) => {
    const first = line.split('|');
    let second = first.pop(first.length - 1);
    second = second.split(/[\s]+/);
    let final = '';
    for (let i = second.length - 1; i >= 0; i -= 1) {
      if (!second[i].includes('=')) {
        second[i - 1] = `${second[i - 1]} ${second[i]}`;
        second.splice(i, 1);
      }
    }
    if (header.split(',').length <= 7) {
      second.forEach((e) => {
        if (!e.startsWith('cs')) {
          const ext = e.split('=');
          header = `${header},extension_${ext[0]}`;
        } else if (e.split('=')[0].endsWith('Label')) {
          const ext = e.split('=');
          header = `${header},extension_${ext[1]}`;
        }
      });
      csv = `${header.replace(/ /g, '_')}`;
    }
    second.forEach((e) => {
      if (!e.startsWith('cs') || (e.startsWith('cs') && !e.split('=')[0].endsWith('Label'))) {
        const position = e.indexOf('=');
        final = `${final}${e.substring(position + 1)},`;
      }
    });
    final = final.substring(0, final.length - 1);
    const l = `${first.join()},${final}`;
    csv = `${csv}\n${l}`;
  });
  return csv;
};

