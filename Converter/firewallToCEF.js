/**
 * File: firewallToCEF.js
 * File Created: Friday, 3rd August 2018 9:57:58 am
 * Author: Rihab Ben Hamouda (rihab.benh@gripdocket.com)
 * -----
 * Last Modified: Wednesday, 21st November 2018 11:51:40 am
 * Modified By: Rihab Ben Hamouda (rihab.benh@gripdocket.com)
 * -----
 * GridPocket SAS Copyright (C) 2018 All Rights Reserved
 * This source is property of GridPocket SAS.
 * Please email contact@gridpocket.com for more information.
 */


const fs = require('fs');
const crypto = require('crypto');
const eol = require('eol');

let cef = [];

function toCEF(fe) {
  // fe.shift();
  fe.pop();
  fe.forEach((line) => {
    let l = line.replace(/, /g, ' ');
    l = l.split('|');
    for (let i = 0; i < l.length; i += 1) {
      if (l[i] === '') {
        l[i] = '""';
      }
    }
    let res = [];
    res[0] = `rt=${l[0]}`;
    res[1] = ` start=${l[1]}`;
    res[2] = ` dvchost=${l[2]}`;
    res[3] = ` reason=${l[3]} TRendMicroDs`;
    res[4] = ` Tags= ${l[4]}`;
    res[5] = ` act=${l[5]}`;
    res[6] = ` cs1=${l[6]}`;
    res[7] = ' cs1Label=Rank';
    res[8] = ` deviceDirection=${l[7]}`;
    res[9] = ` deviceInboundInterface=${l[8]}`;
    res[10] = ` TrendMicroDsFrameType=${l[9]}`;
    res[11] = ` proto=${l[10]}`;
    res[12] = ` cs2=${l[11]}`;
    res[13] = ' cs2Label=TCP Flags';
    res[14] = ` src=${l[12]}`;
    res[15] = ` smac=${l[13]}`;
    res[16] = ` spt=${l[14]}`;
    res[17] = ` dst=${l[15]}`;
    res[18] = ` dmac=${l[16]}`;
    res[19] = ` dpt=${l[17]}`;
    res[20] = ` out=${l[18]}`;
    res[21] = ` cnt=${l[19]}`;
    res[22] = ` end=${l[20]}`;
    res[23] = ` cs3=${l[21]}`;
    res[24] = ' cs3Label=Flow';
    res[25] = ` cs4=${l[22]}`;
    res[26] = ' cs4Label=Status';
    res[27] = ` msg=${l[23]}`;
    res[28] = ` cs5=${l[24]}`;
    res[29] = ' cs5Label=Data Flags';
    res[30] = ` cs6=${l[25]}`;
    res[31] = ' cs6Label=Data Index';
    res[32] = ` TrendMicroDsPacketData=${l[26]}`;
    res[33] = ` cat=${l[27]}`;

    res[0] = res[0].replace(/\"/g, '');
    res[22] = res[22].replace(/\"/g, '');
    res = res.toString().replace(/,/g, '');
    res = res.toString().replace(/  \r/g, '');

    cef.push(`CEF:0|Trend Micro|Deep Security Manager|9.6|20|MSS Firewall Event|3|${res}`);
  });
}

exports.convertToJSON = (req, res) => {
  new Promise((resolve, reject) => {
    const { path } = req.file;
    fs.readFile(path, 'utf8', (err, data) => {
      if (err) {
        return reject(res.sendStatus(404));
      }
      const correctFormat = eol.lf(data);
      const fe = correctFormat.split('\n');
      toCEF(fe);
      fs.unlinkSync(path);
      return resolve();
    });
  }).then(() => {
    const json2 = {
      spec_version: '2.0',
      type: 'stix-bundle',
      id: 'stix-bundle--hash',
      objects: [
        {
          type: 'observed-data',
          id: `observed-data--${crypto.createHmac('sha1', JSON.stringify(cef)).digest('hex')}`,
          created: new Date(),
          modified: new Date(),
          first_observed: new Date(),
          last_observed: new Date(),
          cybox: {
            spec_version: '3.0',
            objects: [
              {
                items: [cef],
              },
            ],
          },
        },
      ],
    };
    cef = [];
    json2.id = `stix-bundle--${crypto.createHmac('sha1', JSON.stringify(json2)).digest('hex')}`;
    const finalJSON = json2;
    res.json(finalJSON);
  })
    .catch((err) => {
      console.error('Error during process', err);
      res.status(500).send(err);
    });
};
