/**
 * File: authLog.js
 * File Created: Monday, 26th November 2018 4:49:34 pm
 * Author: Rihab Ben Hamouda (rihab.benh@gripdocket.com)
 * -----
 * Last Modified: Monday, 10th December 2018 10:24:29 am
 * Modified By: Rihab Ben Hamouda (rihab.benh@gripdocket.com)
 * -----
 * GridPocket SAS Copyright (C) 2018 All Rights Reserved
 * This source is property of GridPocket SAS.
 * Please email contact@gridpocket.com for more information.
 */

const fs = require('fs');
const crypto = require('crypto');
const eol = require('eol');


let cef = [];
let metaData;
let year;

function toCEF(fe) {
  metaData = fe.shift();
  metaData = metaData.split(';');
  year = metaData[1].split('=')[1].split('-')[0];
  fe.pop();
  fe.forEach((line) => {
    let l = line.replace(/, /g, ' ');
    l = l.split(' ');
    for (let i = 0; i < l.length; i += 1) {
      if (l[i] === '') {
        l[i] = '""';
      }
    }
    let res = [];
    l.splice(1, 1);
    if (l[5].startsWith('Accepted')) {
      res[0] = `src=${l[10]}`;
      res[1] = `app=${l[13].replace(':', '')}`;
      res[2] = `duser=${l[8]}`;
      res[3] = `spt=${l[12]}`;
      if (l[13].endsWith(':')) {
        res[4] = 'type=1';
      } else {
        res[4] = 'type=0';
      }
      res[5] = 'outcome=accepted';
      res[6] = `date=${new Date(`${l[0]} ${l[1]} ${l[2]} `).setYear(year)}`;
      res[7] = 'dtz=Europe/Rome';
      res = res.toString().replace(/,/g, ' ').replace(/undefined/g, '""');
      cef.push(`CEF:0|vendor|hostname|1.0|100|ssh-${metaData[2].replace('endTime=', '').replace(' ', '')}|1|${res}`);
    } else if (l[5] === 'pam_unix(sshd:auth):' && l[6] !== 'check') {
      res[0] = `src=${l[13].replace('rhost=', '')}`;
      res[1] = 'app=ssh2';
      res[2] = `d${l[15]}`;
      res[3] = 'type=0';
      res[4] = 'reason=bad_password';
      res[5] = 'outcome=failure';
      res[6] = `date=${new Date(`${l[0]} ${l[1]} ${l[2]} `).setYear(year)}`;
      res[7] = 'dtz=Europe/Rome';
      res = res.toString().replace(/,/g, ' ').replace(/undefined/g, '""');
      cef.push(`CEF:0|vendor|hostname|1.0|100|ssh-${metaData[2].replace('endTime=', '').replace(' ', '')}|5|${res}`);
    } else if (l[5].startsWith('Failed') && l[8] === 'invalid') {
      res[0] = `src=${l[13]}`;
      res[1] = `duser=${l[10]}`;
      res[2] = `app=${l[15]}`;
      res[3] = `spt=${l[14]}`;
      res[4] = 'type=0';
      res[5] = `reason=${l[8]}_${l[9]}`;
      res[6] = 'outcome=failure';
      res[7] = `date=${new Date(`${l[0]} ${l[1]} ${l[2]} `).setYear(year)}`;
      res[8] = 'dtz=Europe/Rome';
      res = res.toString().replace(/,/g, ' ').replace(/undefined/g, '""');
      cef.push(`CEF:0|vendor|hostname|1.0|100|ssh-${metaData[2].replace('endTime=', '').replace(' ', '')}|10|${res}`);
    }
  });
}

exports.convertToJSON = (req, res) => {
  new Promise((resolve, reject) => {
    const { path } = req.file;
    fs.readFile(path, 'utf8', (err, data) => {
      if (err) {
        return reject(res.sendStatus(404));
      }
      const correctFormat = eol.lf(data);
      const fe = correctFormat.split('\n');
      toCEF(fe);
      fs.unlinkSync(path);
      return resolve();
    });
  }).then(() => {
    const json2 = {
      spec_version: '2.0',
      type: 'stix-bundle',
      id: 'stix-bundle--hash',
      objects: [
        {
          type: 'observed-data',
          id: `observed-data--${crypto.createHmac('sha1', JSON.stringify(cef)).digest('hex')}`,
          created: metaData[1].replace('startTime=', '').replace(' ', ''),
          modified: metaData[1].replace('startTime=', '').replace(' ', ''),
          first_observed: metaData[1].replace('startTime=', '').replace(' ', ''),
          last_observed: metaData[2].replace('endTime=', '').replace(' ', ''),
          cybox: {
            spec_version: '3.0',
            objects: [
              {
                type: 'array',
                minitems: 1,
                items: [cef],
              },
            ],
          },
        },
      ],
    };
    cef = [];
    json2.id = `stix-bundle--${crypto.createHmac('sha1', JSON.stringify(json2)).digest('hex')}`;
    const finalJSON = json2;
    res.json(finalJSON);
  })
    .catch((err) => {
      console.error('Error during process', err);
      res.status(500).send(err);
    });
};
