/**
 * File: sendLogs.js
 * File Created: Thursday, 29th April 2019 09:04:46 am
 * Author: Rihab Ben Hamouda (rihab.benh@gripdocket.com)
 * -----
 * Last Modified: Friday, 1Oth May 2019 03:32:08 pm
 * Modified By: Rihab Ben Hamouda (rihab.benh@gripdocket.com)
 * -----
 * GridPocket SAS Copyright (C) 2019 All Rights Reserved
 * This source is property of GridPocket SAS.
 * Please email contact@gridpocket.com for more information.
 */

const request = require('request');
const url = require('../converter.js');

exports.sendLogs = (req, bound, codeRes) => {
  let msg;
  if (bound === 0) {
    msg = `CEF:0|C3ISP|/format-adapter|1.0.0|${req.method}|Request to ${req.method} on ${req.originalUrl}|9|app=${req.protocol.toUpperCase()}/${req.httpVersion} msg=${req.method} on ${req.originalUrl} request=${req.protocol}://${req.get('host')}${req.originalUrl} dst=${req.connection.localAddress.split(':')[3]} src=${req.connection.remoteAddress.split(':')[3]} requestMethod=${req.method} deviceDirection=0 dhost=${req.get('host').split(':')[0]} shost=${req.headers.host.split(':')[0]}`;
  } else if (bound === 1 && codeRes === 200) {
    msg = `CEF:0|C3ISP|/format-adapter|1.0.0|${req.method}|Response to ${req.method} on ${req.originalUrl}|9|app=${req.protocol.toUpperCase()}/${req.httpVersion} msg=${req.method} on ${req.originalUrl} request=${req.protocol}://${req.get('host')}${req.originalUrl} dst=${req.connection.localAddress.split(':')[3]} src=${req.connection.remoteAddress.split(':')[3]} requestMethod=${req.method} deviceDirection=1 dhost=${req.get('host').split(':')[0]} outcome=Success shost=${req.headers.host.split(':')[0]} reason=${codeRes}`;
  } else if (bound === 1 && codeRes !== 200) {
    msg = `CEF:0|C3ISP|/format-adapter|1.0.0|${req.method}|Response to ${req.method} on ${req.originalUrl}|9|app=${req.protocol.toUpperCase()}/${req.httpVersion} msg=${req.method} on ${req.originalUrl} request=${req.protocol}://${req.get('host')}${req.originalUrl} dst=${req.connection.localAddress.split(':')[3]} src=${req.connection.remoteAddress.split(':')[3]} requestMethod=${req.method} deviceDirection=1 dhost=${req.get('host').split(':')[0]} outcome=Failure shost=${req.headers.host.split(':')[0]} reason=${codeRes}`;
  }


  const options = {
    method: 'POST',
    url: url.auditManagerUrl,
    headers:
    {
      'cache-control': 'no-cache',
      'Content-Type': 'application/json',
      authorization: 'Basic dXNlcjpwYXNzd29yZA==',
      accept: 'application/json',
    },
    body:
    {
      endOfBatch: true,
      level: 'INFO',
      loggerFqcn: 'com.hpe.c3isp.logger.auditlog',
      loggerName: 'auditlog',
      message: msg,
      thread: 'http-nio-8443-exec-71',
      timeMillis: Date.now(),
    },
    json: true,
  };

  request(options, (error) => {
    if (error) throw new Error(error);
    console.log(options.body.message);
    console.log(options.url);

  });
};
